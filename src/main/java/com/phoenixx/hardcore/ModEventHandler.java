package com.phoenixx.hardcore;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.init.Blocks;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.world.BlockEvent;

/**
 * @author Phoenixx
 * HardcoreMod-1.7.10
 * 2020-06-24
 * 6:41 PM
 */
public class ModEventHandler {
    @SubscribeEvent
    public void burnOutTorchesPart1(BlockEvent.PlaceEvent event) {
        if (event.block == Blocks.torch) {
            event.world.scheduleBlockUpdate(event.x,event.y,event.z,Blocks.torch,400);
        }
    }

    @SubscribeEvent
    public void multiplyLava(LivingHurtEvent event) {
        if (event.source == DamageSource.lava) {
            event.ammount = (event.ammount * 10);
        }

        Entity attacker = event.source.getSourceOfDamage();
        if (attacker instanceof EntitySilverfish) {
            for (int i = 0 ; i < 3; i++) {
                EntitySilverfish silverfish1 = new EntitySilverfish(event.entityLiving.worldObj);
                silverfish1.setLocationAndAngles(attacker.posX + 0.5D, attacker.posY, attacker.posZ + 0.5D, 0.0F, 0.0F);
                event.entityLiving.worldObj.spawnEntityInWorld(silverfish1);
            }
        }
    }

    @SubscribeEvent
    public void noPigsOrBats(EntityJoinWorldEvent event){
        if (event.entity instanceof EntityPig || event.entity instanceof EntityBat){
            event.setCanceled(true);
        }

        if (event.entity instanceof EntityCreeper) {
            event.entity.onStruckByLightning(null);
        }
    }
}
