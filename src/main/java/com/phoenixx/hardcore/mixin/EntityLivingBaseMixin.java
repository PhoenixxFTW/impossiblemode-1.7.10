package com.phoenixx.hardcore.mixin;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.DamageSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;

@Mixin(EntityLivingBase.class)
public class EntityLivingBaseMixin {

	@ModifyConstant(method = "onEntityUpdate",constant = @Constant(intValue = 300))
	private int shortBreath(int old) {
		return 40;
	}

	@Redirect(method = "onEntityUpdate",at = @At(value = "INVOKE",target = "Lnet/minecraft/entity/EntityLivingBase;attackEntityFrom(Lnet/minecraft/util/DamageSource;F)Z"))
	private boolean instantDrown(EntityLivingBase entityLivingBase, DamageSource damageSource, float damage){
		if(damageSource == DamageSource.drown){
			entityLivingBase.attackEntityFrom(damageSource, Float.MAX_VALUE);
			return true;
		} else {
			entityLivingBase.attackEntityFrom(damageSource, damage);
		}
		return true;
	}
}
