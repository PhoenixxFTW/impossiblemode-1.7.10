package com.phoenixx.hardcore;

import net.minecraft.block.Block;
import net.minecraft.block.BlockTorch;
import net.minecraft.world.World;

import java.util.Random;

/**
 * @author Phoenixx
 * HardcoreMod-1.7.10
 * 2020-06-24
 * 9:38 PM
 */
public class UnLitTorchBlock extends BlockTorch {
    protected UnLitTorchBlock() {
        super();
        setBlockName("unlit_torch");
        setBlockTextureName("impossiblemode:unlit_torch");
    }

    @Override
    public void randomDisplayTick(World p_149734_1_, int p_149734_2_, int p_149734_3_, int p_149734_4_, Random p_149734_5_) {
    }
}
