package com.phoenixx.hardcore.mixin;

import com.phoenixx.hardcore.HardcoreMod;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * @author Phoenixx
 * HardcoreMod-1.7.10
 * 2020-06-24
 * 6:44 PM
 */
@Mixin(EntitySkeleton.class)
public class EntitySkeletonMixin extends EntityMob {
    public EntitySkeletonMixin(World worldIn) {
        super(worldIn);
    }

    @Redirect(method = "onLivingUpdate",at = @At(value = "INVOKE",target = "Lnet/minecraft/entity/monster/EntitySkeleton;setFire(I)V"))
    private void noBurn(EntitySkeleton entitySkeleton, int seconds) {
    }

    @Redirect(method = "attackEntityWithRangedAttack",at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/EntityArrow;setDamage(D)V"))
    private void setDamage(EntityArrow entityArrow, double old) {
        entityArrow.setDamage(old*2);
        entityArrow.setFire(100);
    }
}
