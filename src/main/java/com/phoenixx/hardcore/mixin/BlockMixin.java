package com.phoenixx.hardcore.mixin;

import com.phoenixx.hardcore.HardcoreMod;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

@Mixin(Block.class)
public class BlockMixin {
	@Inject(method = "updateTick",at = @At("RETURN"))
	private void burnOutTorchesPart2(World world, int posX, int posY, int posZ, Random p_149674_5_, CallbackInfo ci) {
		Block block = (Block)(Object)this;
		if (block == Blocks.torch) {
			world.setBlock(posX,posY,posZ, HardcoreMod.unlit_torch);
		}
	}
	@Inject(method = "onEntityWalking",at = @At("RETURN"))
	private void wee(World world, int p_149724_2_, int p_149724_3_, int p_149724_4_, Entity entity, CallbackInfo ci){
		Block block = (Block)(Object)this;
		if (block == Blocks.ice) {
			entity.motionY = 5;
		} else if (block == Blocks.packed_ice){
			entity.motionY = 10;
		}
	}
}
