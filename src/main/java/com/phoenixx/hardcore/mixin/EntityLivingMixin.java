package com.phoenixx.hardcore.mixin;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityLiving.class)
abstract class EntityLivingMixin extends EntityLivingBase {

	public EntityLivingMixin(World worldIn) {
		super(worldIn);
	}

	@Inject(method = "onSpawnWithEgg", at = @At("RETURN"))
	private void alwaysEquip(IEntityLivingData livingData, CallbackInfoReturnable<IEntityLivingData> cir) {
		if ((Object) this instanceof EntityZombie) {
			EntityZombie entityZombie = (EntityZombie)((Object) this);
			entityZombie.setChild(true);
			int i = this.rand.nextInt(2);

			++i;

			++i;

			++i;

			for(int j=1;j<5;j++){
                ItemStack itemstack = this.getEquipmentInSlot(j);

                if (itemstack == null) {
                    Item item = EntityLiving.getArmorItemForSlot(j, i);

                    if (item != null) {
                        this.setCurrentItemOrArmor(j, new ItemStack(item));
                    }
                }
            }
		}
	}
}
