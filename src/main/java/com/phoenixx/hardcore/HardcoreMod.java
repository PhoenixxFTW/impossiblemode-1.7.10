package com.phoenixx.hardcore;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.init.Blocks;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.world.BlockEvent;

import java.util.logging.Logger;

@Mod(modid = HardcoreMod.MOD_ID, version = HardcoreMod.VERSION)
public class HardcoreMod
{
    public static final String MOD_ID = "impossiblemode";
    public static final String MOD_NAME = "Impossible Mode";
    public static final String VERSION = "1.0";

    public static Logger LOGGER = Logger.getLogger(MOD_NAME);

    public static Block unlit_torch = new UnLitTorchBlock();

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

        LOGGER.info("Pssttt.... I like easter eggs, don't you?");

        MinecraftForge.EVENT_BUS.register(new ModEventHandler());
        FMLCommonHandler.instance().bus().register(new ModEventHandler());

        unlit_torch.setCreativeTab(CreativeTabs.tabBlock);

        // register blocks
        GameRegistry.registerBlock(unlit_torch, "unlit_torch");
    }
}
